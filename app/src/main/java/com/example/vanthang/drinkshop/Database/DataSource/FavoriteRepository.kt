package com.example.vanthang.drinkshop.Database.DataSource

import com.example.vanthang.drinkshop.Database.ModelDB.Favorite
import io.reactivex.Flowable

class FavoriteRepository(private val iFavoriteDataSource: IFavoriteDataSource) : IFavoriteDataSource {


    companion object {
        private var instance:FavoriteRepository?=null
        fun getInstance(iFavoriteDataSource:IFavoriteDataSource):FavoriteRepository
        {
            if (instance==null)
                instance= FavoriteRepository(iFavoriteDataSource)
            return instance!!
        }

    }

    override fun getFavoriteItems(): Flowable<List<Favorite>> {
        return iFavoriteDataSource!!.getFavoriteItems()
    }

    override fun isFavorite(itemId: Int): Int {
        return iFavoriteDataSource!!.isFavorite(itemId)
    }

    override fun insertFavorite(vararg favorites: Favorite) {
        return iFavoriteDataSource!!.insertFavorite(*favorites)
    }
    override fun delete(favorite: Favorite) {
        return iFavoriteDataSource!!.delete(favorite)
    }
}