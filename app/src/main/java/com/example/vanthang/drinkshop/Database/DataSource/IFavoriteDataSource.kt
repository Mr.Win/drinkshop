package com.example.vanthang.drinkshop.Database.DataSource

import com.example.vanthang.drinkshop.Database.ModelDB.Favorite
import io.reactivex.Flowable

interface IFavoriteDataSource {

    fun getFavoriteItems(): Flowable<List<Favorite>>

    fun isFavorite(itemId:Int):Int

    fun insertFavorite(vararg favorites: Favorite)

    fun delete(favorite: Favorite)
}