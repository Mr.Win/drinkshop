package com.example.vanthang.drinkshop.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.vanthang.drinkshop.Database.ModelDB.Favorite
import com.example.vanthang.drinkshop.R
import com.squareup.picasso.Picasso
import java.lang.StringBuilder


class FavoriteAdapter : RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {
    private var context:Context?=null
    private var favoriteList:ArrayList<Favorite>?=null
    constructor(context: Context,favoriteList: ArrayList<Favorite>)
    {
        this.context=context
        this.favoriteList=favoriteList
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view=LayoutInflater.from(context).inflate(R.layout.item_favorite_layout,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return favoriteList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.with(context)
                .load(favoriteList!![position].link)
                .into(holder.img_favorite)
        holder.txt_favorite_order!!.text=favoriteList!![position].name
        holder.txt_favorite_price!!.text=StringBuilder("$").append(favoriteList!![position].price)

    }

    public class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        var img_favorite: ImageView?=null
        var txt_favorite_order: TextView?=null
        var txt_favorite_price:TextView?=null
        var view_background_favorite:RelativeLayout?=null
        var view_foreground_favorite:LinearLayout?=null

        init {
            img_favorite=itemView.findViewById(R.id.img_favorite)
            txt_favorite_order=itemView.findViewById(R.id.txt_favorite_order)
            txt_favorite_price=itemView.findViewById(R.id.txt_favorite_price)
            view_background_favorite=itemView.findViewById(R.id.view_background_favorite)
            view_foreground_favorite=itemView.findViewById(R.id.view_foreground_favorite)
        }
    }
    fun removeItem(position: Int)
    {
        favoriteList!!.removeAt(position)
        notifyItemRemoved(position)
    }
    fun restoreItem(item:Favorite,position: Int)
    {
        favoriteList!!.add(position,item)
        notifyItemInserted(position)
    }
}