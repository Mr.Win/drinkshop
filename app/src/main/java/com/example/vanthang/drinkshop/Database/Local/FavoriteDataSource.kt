package com.example.vanthang.drinkshop.Database.Local

import com.example.vanthang.drinkshop.Database.DataSource.IFavoriteDataSource
import com.example.vanthang.drinkshop.Database.ModelDB.Favorite
import io.reactivex.Flowable

class FavoriteDataSource : IFavoriteDataSource {


    private var favoriteDAO:FavoriteDAO?=null
    constructor(favoriteDAO: FavoriteDAO)
    {
        this.favoriteDAO=favoriteDAO
    }
    companion object {
        var instance:FavoriteDataSource?=null
        fun getInstance(favoriteDAO: FavoriteDAO):FavoriteDataSource
        {
            if (instance==null)
                instance= FavoriteDataSource(favoriteDAO)
            return instance!!
        }
    }
    override fun getFavoriteItems(): Flowable<List<Favorite>> {
        return favoriteDAO!!.getFavoriteItems()
    }

    override fun isFavorite(itemId: Int): Int {
        return favoriteDAO!!.isFavorite(itemId)
    }

    override fun insertFavorite(vararg favorites: Favorite) {
        return favoriteDAO!!.insertFavorite(*favorites)
    }

    override fun delete(favorite: Favorite) {
        return favoriteDAO!!.delete(favorite)
    }
}