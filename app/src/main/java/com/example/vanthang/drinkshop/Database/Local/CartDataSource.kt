package com.example.vanthang.drinkshop.Database.Local

import com.example.vanthang.drinkshop.Database.DataSource.ICartDataSource
import com.example.vanthang.drinkshop.Database.ModelDB.Cart
import io.reactivex.Flowable

class CartDataSource:ICartDataSource {


    private var cartDAO:CartDAO?=null
    constructor(cartDAO: CartDAO)
    {
        this.cartDAO=cartDAO
    }
    companion object {
        var instance:CartDataSource?=null
        fun getInstance(cartDAO: CartDAO):CartDataSource
        {
            if (instance==null)
                instance= CartDataSource(cartDAO)
            return instance!!
        }
    }

    override fun getCartItems(): Flowable<List<Cart>> {
        return cartDAO!!.getCartItems()
    }

    override fun getCartItemById(cartItemId: Int): Flowable<List<Cart>> {
        return cartDAO!!.getCartItemById(cartItemId)
    }

    override fun countCartItems(): Int {
        return cartDAO!!.countCartItems()
    }

    override fun emptyCart() {
        cartDAO!!.emptyCart()
    }

    override fun insertToCart(vararg carts: Cart) {
        cartDAO!!.insertToCart(*carts)
    }

    override fun updateToCart(vararg carts: Cart) {
        cartDAO!!.updateToCart(*carts)
    }

    override fun deleteCartItem(carts: Cart) {
        cartDAO!!.deleteCartItem(carts)
    }
    override fun sumPrice(): Float {
       return cartDAO!!.sumPrice()
    }

}