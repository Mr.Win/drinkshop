package com.example.vanthang.drinkshop.Retrofit


import com.example.vanthang.drinkshop.Model.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*


interface IDrinkShopAPI {
    @FormUrlEncoded
    @POST("checkuser.php")
    fun checkUserExists(@Field("phone") phone: String): Call<CheckUserResponse>

    @FormUrlEncoded
    @POST("register.php")
    fun registerNewUser(@Field("phone") phone: String,
                        @Field("name") name: String,
                        @Field("birthdate") birthdate: String,
                        @Field("address") address: String): Call<User>

    @FormUrlEncoded
    @POST("getdrink.php")
    fun getDrink(@Field("menuid") menuid: String): Observable<List<Drinks>>


    @FormUrlEncoded
    @POST("getinforuser.php")
    fun getUserInformation(@Field("phone") phone: String): Call<User>


    @GET("getbanners.php")
    fun getBanners(): Observable<List<Banner>>

    @GET("getmenu.php")
    fun getMenu(): Observable<List<Category>>

    @Multipart
    @POST("uploadavatar.php")
    fun uploadFile(@Part phone: MultipartBody.Part,@Part file: MultipartBody.Part): Call<String>

    @GET("getalldrinks.php")
    fun getAllDrinks(): Observable<List<Drinks>>

    @FormUrlEncoded
    @POST("submitorder.php")
    fun submitOrder(@Field("userPhone") userPhone: String,
                    @Field("orderDetail") orderDetail: String,
                    @Field("orderComment") orderComment: String,
                    @Field("orderAddress") orderAddress: String,
                    @Field("orderPrice") orderPrice: Float,
                    @Field("paymentMethod")paymentMethod:String): Call<OrderResult>

    @FormUrlEncoded
    @POST("braintree/checkout.php")
    fun payment(@Field("nonce") nonce: String,
                @Field("amount") amount: String): Call<String>

    //Status of order

    @FormUrlEncoded
    @POST("getorder.php")
    fun getOrderStatus(@Field("userPhone") userPhone:String,
                       @Field("status")status:String):Observable<List<Order>>

    @FormUrlEncoded
    @POST("updatetoken.php")
    fun updateToken(@Field("phone")phone:String,
                    @Field("token")token:String,
                    @Field("isServerToken")isServerToken:String) : Call<String>

    @FormUrlEncoded
    @POST("cancelorder.php")
    fun cancelOrder(@Field("orderId")orderId:String,
                    @Field("userPhone")userPhone:String):Call<String>

    //get nearby store
    @FormUrlEncoded
    @POST("getnearbystore.php")
    fun getNearbyStore(@Field("lat")lat:String,
                       @Field("lng")lng:String):Observable<List<Store>>

    @FormUrlEncoded
    @POST("gettoken.php")
    fun getToken(@Field("phone")phone:String,
                 @Field("isServerToken")isServerToken:String):Call<Token>
}