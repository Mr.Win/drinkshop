package com.example.vanthang.drinkshop.Model

class Order {
    var OrderId:Long?=null
    var OderStatus:Int?=null
    var OderPrice:Float?=null
    var OderDetail:String?=null
    var OderComment:String?=null
    var OderAddress:String?=null
    var UserPhone:String?=null
    var PaymentMethod:String?=null
}