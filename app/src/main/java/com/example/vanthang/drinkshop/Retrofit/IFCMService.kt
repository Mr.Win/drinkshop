package com.example.vanthang.drinkshop.Retrofit

import com.example.vanthang.drinkshop.Model.DataMessage
import com.example.vanthang.drinkshop.Model.MyResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface IFCMService {
    @Headers(
        "Content-Type:application/json",
        "Authorization:key=AAAAWZ4NGMQ:APA91bFTK6amwqZnDNbmifyVnpNgyYc3tgdH-_3YS_kQzMGKSBpotYlEDQlcRks-Er5EpJtI7bITlOeGFkf3JOTaVY6VRp-KaPhIcj2BowjuRjtD5LrkaZA7ForugaJTwYylVbVIk37l"
    )
    @POST("fcm/send")
    fun sendNotification(@Body body: DataMessage):Call<MyResponse>
}