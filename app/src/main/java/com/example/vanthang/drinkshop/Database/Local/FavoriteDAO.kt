package com.example.vanthang.drinkshop.Database.Local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.example.vanthang.drinkshop.Database.ModelDB.Favorite
import io.reactivex.Flowable

@Dao
interface FavoriteDAO {
    @Query("SELECT * FROM Favorite")
    fun getFavoriteItems(): Flowable<List<Favorite>>

    @Query("SELECT EXISTS (SELECT 1 FROM Favorite WHERE id=:itemId)")
    fun isFavorite(itemId:Int):Int

    @Insert
    fun insertFavorite(vararg favorites: Favorite)

    @Delete
    fun delete(favorite: Favorite)
}