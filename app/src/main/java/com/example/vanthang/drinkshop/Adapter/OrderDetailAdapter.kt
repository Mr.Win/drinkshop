package com.example.vanthang.drinkshop.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.example.vanthang.drinkshop.Database.ModelDB.Cart
import com.example.vanthang.drinkshop.R
import com.squareup.picasso.Picasso

class OrderDetailAdapter:RecyclerView.Adapter<OrderDetailAdapter.ViewHolder> {

    private var context: Context?=null
    private var cartList:List<Cart>?=null
    constructor(context: Context,cartList: List<Cart>)
    {
        this.context=context
        this.cartList=cartList
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view= LayoutInflater.from(context).inflate(R.layout.item_order_detail_layout,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return cartList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.with(context)
                .load(cartList!![position].link)
                .into(holder.img_order)
        holder.txt_order_name!!.text=StringBuilder(cartList!![position].name)
                .append(if (cartList!![position].size==0) " Size M" else " Size L")
                .append(" x")
                .append(cartList!![position].amount).toString()
        holder.txt_order_sugar_ice!!.text=StringBuilder("Sugar: ")
                .append(cartList!![position].sugar.toString())
                .append("%")
                .append("\n")
                .append("Ice: ")
                .append(cartList!![position].ice.toString())
                .append("%").toString()
        holder.txt_order_price!!.text=StringBuilder("$").append(cartList!![position].price.toString())

    }

    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        var img_order: ImageView?=null
        var txt_order_name: TextView?=null
        var txt_order_sugar_ice:TextView?=null
        var txt_order_price:TextView?=null
        init {
            img_order=itemView.findViewById(R.id.img_order)
            txt_order_name=itemView.findViewById(R.id.txt_order_name)
            txt_order_sugar_ice=itemView.findViewById(R.id.txt_order_sugar_ice)
            txt_order_price=itemView.findViewById(R.id.txt_order_price)
        }
    }
}