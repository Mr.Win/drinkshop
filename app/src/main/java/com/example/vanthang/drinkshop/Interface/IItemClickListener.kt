package com.example.vanthang.drinkshop.Interface

import android.view.View

interface IItemClickListener {
    fun onClick(v:View)
}