package com.example.vanthang.drinkshop

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.vanthang.drinkshop.Adapter.OrderDetailAdapter
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.Database.ModelDB.Cart
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_order_detail.*
import retrofit2.Call
import retrofit2.Response

class OrderDetailActivity : AppCompatActivity() {

    lateinit var orderDetailAdapter: OrderDetailAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_detail)

        txt_show_order_id!!.text=StringBuilder("#").append(Common.current_Order_Detail!!.OrderId).toString()
        txt_show_order_price!!.text=StringBuilder("$").append(Common.current_Order_Detail!!.OderPrice).toString()
        txt_show_order_address!!.text=Common.current_Order_Detail!!.OderAddress
        txt_show_order_comment!!.text=Common.current_Order_Detail!!.OderComment
        txt_show_order_status!!.text=StringBuilder("Order Status: ").append(Common.convertCodeToStatus(Common.current_Order_Detail!!.OderStatus))
        if(Common.current_Order_Detail!!.OderStatus==0)
            btn_cancel.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {
                    cancelOrder()
                }
            })
        else
            btn_cancel.isEnabled=false

        rw_order_detail.layoutManager=LinearLayoutManager(this@OrderDetailActivity,LinearLayoutManager.VERTICAL,false)
        rw_order_detail.hasFixedSize()

        displayOrderDetail()
    }

    private fun cancelOrder() {
        var mService=Common.getAPI()
        mService.cancelOrder(Common.current_Order_Detail!!.OrderId.toString(), Common.currentUser!!.phone!!)
                .enqueue(object :retrofit2.Callback<String>{
                    override fun onFailure(call: Call<String>, t: Throwable) {
                        Log.d("DEBUG",t.message)
                    }

                    override fun onResponse(call: Call<String>, response: Response<String>) {
                        Toast.makeText(this@OrderDetailActivity,response.body(),Toast.LENGTH_SHORT).show()
                        if (response.body()!!.contains("Order has been cancelled"))
                            finish()
                    }
                })
    }

    private fun displayOrderDetail() {
        var orderDetail:List<Cart> = Gson().fromJson(Common.current_Order_Detail!!.OderDetail ,object : TypeToken<List<Cart>>() {}.type)
        orderDetailAdapter=OrderDetailAdapter(this@OrderDetailActivity,orderDetail!!)
        rw_order_detail.adapter=orderDetailAdapter
    }
}
