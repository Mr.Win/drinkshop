package com.example.vanthang.drinkshop.Adapter

import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.Database.ModelDB.Cart
import com.example.vanthang.drinkshop.Database.ModelDB.Favorite
import com.example.vanthang.drinkshop.Interface.IItemClickListener
import com.example.vanthang.drinkshop.Model.Drinks
import com.example.vanthang.drinkshop.R
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import java.lang.Exception


class DrinkAdapter(var context:Context,var drinks:List<Drinks>):RecyclerView.Adapter<DrinkAdapter.ViewHolder> (){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view:View=LayoutInflater.from(context).inflate(R.layout.item_drink,null)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return drinks!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Picasso.with(context)
                .load(drinks!![position].Link)
                .into(holder.img_drink)
        holder.txt_drink!!.text=drinks!![position].Name
        holder.txt_price!!.text=StringBuilder("$").append(drinks!![position].Price.toString())
        holder.setItemClickListener(object :IItemClickListener{
            override fun onClick(v: View) {
                Toast.makeText(context,"Click!",Toast.LENGTH_SHORT).show()
            }
        })
        holder.img_add_cart!!.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                showAddToCartDialog(position)
            }
        })
        //favorite
        if (Common.favoriteRepository!!.isFavorite(drinks[position].ID!!.toInt())==1)
            holder.img_add_favorite!!.setImageResource(R.drawable.ic_favorite_white_24dp)
        else
            holder.img_add_favorite!!.setImageResource(R.drawable.ic_favorite_border_white_24dp)
        holder.img_add_favorite!!.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                if (Common.favoriteRepository!!.isFavorite(drinks[position].ID!!.toInt())!=1)
                {
                    addOrRemoveFavorite(drinks[position],true)
                    holder.img_add_favorite!!.setImageResource(R.drawable.ic_favorite_white_24dp)
                }else
                {
                    addOrRemoveFavorite(drinks[position],false)
                    holder.img_add_favorite!!.setImageResource(R.drawable.ic_favorite_border_white_24dp)
                }
            }
        })
    }

    private fun addOrRemoveFavorite(drinks: Drinks, isAdd: Boolean) {
        var favorite=Favorite()
        favorite.id=drinks.ID
        favorite.name=drinks.Name
        favorite.link=drinks.Link
        favorite.price=drinks.Price
        favorite.menuId=drinks.Menuid

        if (isAdd)
            Common.favoriteRepository!!.insertFavorite(favorite)
        else
            Common.favoriteRepository!!.delete(favorite)

    }

    private fun showAddToCartDialog(position: Int) {
        var builder:AlertDialog.Builder=AlertDialog.Builder(context)
        var itemView:View=LayoutInflater.from(context).inflate(R.layout.add_to_cart_layout,null)

        //View
        var img_product_dialog=itemView.findViewById<ImageView>(R.id.img_cart_product)
        var btn_count_dialog=itemView.findViewById<ElegantNumberButton>(R.id.btn_count)
        var txt_product_dialog=itemView.findViewById<TextView>(R.id.txt_cart_product_name)

//        var edt_comment_dialog=itemView.findViewById<EditText>(R.id.edt_comment)

        var rd_sizeM=itemView.findViewById<RadioButton>(R.id.rd_sizeM)
        var rd_sizeL=itemView.findViewById<RadioButton>(R.id.rd_sizeL)
        rd_sizeM.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                    Common.sizeofCup=0
            }

        })
        rd_sizeL.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                    Common.sizeofCup=1
            }

        })

        var rd_sugar_100=itemView.findViewById<RadioButton>(R.id.rd_sugar_100)
        var rd_sugar_70=itemView.findViewById<RadioButton>(R.id.rd_sugar_70)
        var rd_sugar_50=itemView.findViewById<RadioButton>(R.id.rd_sugar_50)
        var rd_sugar_30=itemView.findViewById<RadioButton>(R.id.rd_sugar_30)
        var rd_sugar_0=itemView.findViewById<RadioButton>(R.id.rd_sugar_0)

        rd_sugar_100.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                    Common.sugar=100
            }

        })
        rd_sugar_70.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                    Common.sugar=70
            }

        })
        rd_sugar_50.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                    Common.sugar=50
            }

        })
        rd_sugar_30.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                    Common.sugar=30
            }

        })
        rd_sugar_0.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                    Common.sugar=0
            }

        })

        var rd_ice_100=itemView.findViewById<RadioButton>(R.id.rd_ice_100)
        var rd_ice_70=itemView.findViewById<RadioButton>(R.id.rd_ice_70)
        var rd_ice_50=itemView.findViewById<RadioButton>(R.id.rd_ice_50)
        var rd_ice_30=itemView.findViewById<RadioButton>(R.id.rd_ice_30)
        var rd_ice_0=itemView.findViewById<RadioButton>(R.id.rd_ice_0)

        rd_ice_100.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                    Common.ice=100
            }

        })
        rd_ice_70.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                    Common.ice=70
            }

        })
        rd_ice_50.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                    Common.ice=50
            }

        })
        rd_ice_30.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                    Common.ice=30
            }

        })
        rd_ice_0.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                    Common.ice=0
            }

        })

        var recycler_extra_topping=itemView.findViewById<RecyclerView>(R.id.recycler_extra_topping)
        recycler_extra_topping.layoutManager=LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false)
        recycler_extra_topping.hasFixedSize()

        var adapter:MultiChoiceAdapter=MultiChoiceAdapter(context,Common.toppingList!!)
        recycler_extra_topping.adapter=adapter

        Picasso.with(context)
                .load(drinks!![position].Link)
                .into(img_product_dialog)
        txt_product_dialog.text=drinks[position].Name


        builder.setNegativeButton("ADD TO CART",object :DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                if (Common.sizeofCup==-1)
                {
                    Toast.makeText(context,"Please choice size of cup",Toast.LENGTH_SHORT).show()
                    return
                }
                if (Common.sugar==-1)
                {
                    Toast.makeText(context,"Please choice sugar",Toast.LENGTH_SHORT).show()
                    return
                }
                if (Common.ice==-1)
                {
                    Toast.makeText(context,"Please choice ice",Toast.LENGTH_SHORT).show()
                    return
                }
                showConfirmDialog(position,btn_count_dialog.number)
                dialog!!.dismiss()
            }
        })
        builder.setView(itemView)
        builder.show()

    }

    private fun showConfirmDialog(position: Int, number: String?) {
        var builder:AlertDialog.Builder=AlertDialog.Builder(context)
        var itemView:View=LayoutInflater.from(context).inflate(R.layout.confirm_add_to_cart_layout,null)

        //View
        var img_confirm_product=itemView.findViewById<ImageView>(R.id.img_confirm_product)
        var txt_confirm_product_name=itemView.findViewById<TextView>(R.id.txt_confirm_product_name)
        var txt_confirm_product_price=itemView.findViewById<TextView>(R.id.txt_confirm_product_price)
        var txt_sugar=itemView.findViewById<TextView>(R.id.txt_sugar)
        var txt_ice=itemView.findViewById<TextView>(R.id.txt_ice)
        var txt_topping_extra=itemView.findViewById<TextView>(R.id.txt_topping_extra)

        //Set Data
        Picasso.with(context)
                .load(drinks!![position].Link)
                .into(img_confirm_product)

        txt_confirm_product_name.text=java.lang.StringBuilder(drinks[position].Name)
                .append(if (Common.sizeofCup == 0) " Size M" else " Size L")
                .append(" x")
                .append(number).toString()


        txt_sugar.text=java.lang.StringBuilder("Sugar: ").append(Common.sugar).append("%").toString()
        txt_ice.text=java.lang.StringBuilder("Ice: ").append(Common.ice).append("%").toString()

        var price:Double=(drinks[position].Price!!.toDouble()*number!!.toDouble())+Common.toppingPrice
        if (Common.sizeofCup==1)//Size L
            price +=(2.0*number.toDouble())

        var topping_final_comment=StringBuffer("")
        for (line in Common.toppingAdded)
            topping_final_comment.append(line).append("\n")
        txt_topping_extra.text=topping_final_comment


        txt_confirm_product_price.text=java.lang.StringBuilder("$").append(price).toString()

        builder.setNegativeButton("CONFIRM",object :DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {

                dialog!!.dismiss()
                //Add to SQLite
                //create new Cart item
                try {
                    var cartItem=Cart()
                    cartItem.name=drinks[position].Name
                    cartItem.amount=number.toInt()
                    cartItem.price=price
                    cartItem.ice=Common.ice
                    cartItem.sugar=Common.sugar
                    cartItem.size=Common.sizeofCup
                    cartItem.toppingExtras=txt_topping_extra.text.toString()
                    cartItem.link=drinks[position].Link

                    //Add to DataBase
                    Common.cartRepository!!.insertToCart(cartItem)
                    Log.d("ANT",Gson().toJson(cartItem))
                    Toast.makeText(context,"save item to Cart success",Toast.LENGTH_SHORT).show()
                }catch (e:Exception)
                {
                    Toast.makeText(context,e.message.toString(),Toast.LENGTH_SHORT).show()
                }
            }
        })
        builder.setView(itemView)
        builder.show()
    }


    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView),View.OnClickListener{
        internal var img_drink:ImageView?=null
        internal var txt_drink:TextView?=null
        internal var txt_price:TextView?=null
        internal var img_add_cart:ImageView?=null
        internal var img_add_favorite:ImageView?=null
        private var itemClickListener:IItemClickListener?=null
        init {
            img_drink=itemView.findViewById(R.id.img_drink)
            txt_drink=itemView.findViewById(R.id.txt_drink_name)
            txt_price=itemView.findViewById(R.id.txt_drink_price)
            img_add_cart=itemView.findViewById(R.id.img_add_cart)
            img_add_favorite=itemView.findViewById(R.id.img_add_favorite)
            itemView.setOnClickListener(this)
        }
        fun setItemClickListener(itemClickListener: IItemClickListener)
        {
            this.itemClickListener=itemClickListener
        }
        override fun onClick(v: View?) {
            this.itemClickListener!!.onClick(v!!)
        }
    }
}