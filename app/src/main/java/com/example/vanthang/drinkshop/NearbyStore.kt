package com.example.vanthang.drinkshop

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.widget.Toast
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.Model.Store
import com.example.vanthang.drinkshop.Retrofit.IDrinkShopAPI
import com.google.android.gms.location.*

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import java.lang.StringBuilder


class NearbyStore : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    lateinit var locationCallback: LocationCallback
    lateinit var locationRequest:LocationRequest
    lateinit var mSerVice:IDrinkShopAPI

    //RxJava
    var compositeDisposable=CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nearby_store)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        mSerVice=Common.getAPI()

        Dexter.withActivity(this@NearbyStore)
                .withPermissions(android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object :MultiplePermissionsListener{
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted())
                        {
                            buildLocationRequest()
                            buildLocationCallBack()
                            fusedLocationProviderClient=LocationServices.getFusedLocationProviderClient(this@NearbyStore)

                            //start update location
                            if (ActivityCompat.checkSelfPermission(this@NearbyStore,android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                    ActivityCompat.checkSelfPermission(this@NearbyStore,android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED  )
                                return
                            fusedLocationProviderClient.requestLocationUpdates(locationRequest,locationCallback, Looper.myLooper())
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        Toast.makeText(this@NearbyStore,"Permission Denied",Toast.LENGTH_SHORT).show()
                    }
                }).check()
    }

    private fun buildLocationCallBack() {
        locationCallback = object : LocationCallback(){
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)
                // Add a marker in your location and move the camera
                var yourLocation = LatLng(p0!!.lastLocation.latitude,p0!!.lastLocation.longitude)
                mMap.addMarker(MarkerOptions().position(yourLocation).title("Your Location"))
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(yourLocation,17.0f))

                getNearbyStore(p0!!.lastLocation)
            }
        }
    }

    private fun getNearbyStore(lastLocation: Location?) {
        compositeDisposable.add(mSerVice.getNearbyStore(lastLocation!!.latitude.toString(),lastLocation!!.longitude.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :Consumer<List<Store>>{
                    override fun accept(stores: List<Store>?) {
                        for (store in stores!!)
                        {
                            var storeLocation=LatLng(store.lat!!.toDouble(),store.lng!!.toDouble())
                            mMap.addMarker(MarkerOptions()
                                    .position(storeLocation)
                                    .title(store.name)
                                    .snippet(StringBuilder("Distance: ").append(store.distance_in_km).append(" km").toString())
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_nearby_store)))
                        }
                    }
                },object :Consumer<Throwable>{
                    override fun accept(t: Throwable?) {
                        Log.d("ERROR",t!!.message.toString())
                    }
                }))
    }


    private fun buildLocationRequest() {
        locationRequest= LocationRequest()
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        locationRequest.setInterval(5000)
        locationRequest.setFastestInterval(3000)
        locationRequest.setSmallestDisplacement(10f)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.getUiSettings().setZoomControlsEnabled(true)
    }

    override fun onPause() {
        if (locationCallback != null)
            fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        if (locationCallback != null)
        {
            if (ActivityCompat.checkSelfPermission(this@NearbyStore,android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this@NearbyStore,android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED  )
                return
            fusedLocationProviderClient.requestLocationUpdates(locationRequest,locationCallback, Looper.myLooper())
        }
    }

    override fun onStop() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        compositeDisposable.clear()
        super.onStop()
    }
}
