package com.example.vanthang.drinkshop.Database.DataSource

import com.example.vanthang.drinkshop.Database.ModelDB.Cart
import io.reactivex.Flowable

class CartRepository(private val iCartDataSource:ICartDataSource):ICartDataSource {


    companion object {
        private var instance:CartRepository?=null
        fun getInstance(iCartDataSource:ICartDataSource):CartRepository
        {
            if (instance==null)
                instance= CartRepository(iCartDataSource)
            return instance!!
        }

    }
    override fun getCartItems(): Flowable<List<Cart>> {
        return iCartDataSource!!.getCartItems()
    }

    override fun getCartItemById(cartItemId: Int): Flowable<List<Cart>> {
        return iCartDataSource!!.getCartItemById(cartItemId)
    }

    override fun countCartItems(): Int {
        return iCartDataSource!!.countCartItems()
    }

    override fun emptyCart() {
        iCartDataSource!!.emptyCart()
    }

    override fun insertToCart(vararg carts: Cart) {
        iCartDataSource!!.insertToCart(*carts)
    }

    override fun updateToCart(vararg carts: Cart) {
        iCartDataSource!!.updateToCart(*carts)
    }

    override fun deleteCartItem(carts: Cart) {
        iCartDataSource!!.deleteCartItem(carts)
    }
    override fun sumPrice(): Float {
        return iCartDataSource!!.sumPrice()
    }
}