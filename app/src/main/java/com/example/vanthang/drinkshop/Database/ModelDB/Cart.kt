package com.example.vanthang.drinkshop.Database.ModelDB

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity(tableName = "Cart")
class Cart {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id:Int=0

    @ColumnInfo(name = "name")
    var name:String?=null

    @ColumnInfo(name = "link")
    var link:String?=null

    @ColumnInfo(name = "amount")
    var amount:Int?=null

    @ColumnInfo(name = "price")
    var price:Double?=null

    @ColumnInfo(name = "sugar")
    var sugar:Int?=null

    @ColumnInfo(name = "ice")
    var ice:Int?=null

    @ColumnInfo(name = "size")
    var size:Int?=null

    @ColumnInfo(name = "toppingExtras")
    var toppingExtras:String?=null
}