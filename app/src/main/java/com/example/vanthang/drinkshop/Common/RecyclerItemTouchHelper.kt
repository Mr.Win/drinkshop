package com.example.vanthang.drinkshop.Common

import android.graphics.Canvas
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import com.example.vanthang.drinkshop.Adapter.CartAdapter
import com.example.vanthang.drinkshop.Adapter.FavoriteAdapter




class RecyclerItemTouchHelper : ItemTouchHelper.SimpleCallback {
    private var listener:RecyclerItemTouchHelperListener?=null
    constructor(dragDirs:Int,swipedirs:Int,listener: RecyclerItemTouchHelperListener) : super(dragDirs,swipedirs) {
        this.listener=listener
    }
    override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?): Boolean {
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
        if (listener!=null)
        {
            listener!!.onSwiped(viewHolder!!,direction,viewHolder!!.adapterPosition)
        }
    }

    override fun clearView(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?) {
       if (viewHolder is FavoriteAdapter.ViewHolder)
       {
           var foregroundView = (viewHolder as FavoriteAdapter.ViewHolder).view_foreground_favorite
           ItemTouchHelper.Callback.getDefaultUIUtil().clearView(foregroundView)
       }else if (viewHolder is CartAdapter.ViewHolder)
       {
           var foregroundView = (viewHolder as CartAdapter.ViewHolder).view_foreground_cart
           ItemTouchHelper.Callback.getDefaultUIUtil().clearView(foregroundView)
       }
    }

    override fun convertToAbsoluteDirection(flags: Int, layoutDirection: Int): Int {
        return super.convertToAbsoluteDirection(flags, layoutDirection)
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        if (viewHolder !=null)
        {
           if (viewHolder is FavoriteAdapter.ViewHolder)
           {
               var foregroundView = (viewHolder as FavoriteAdapter.ViewHolder).view_foreground_favorite
               ItemTouchHelper.Callback.getDefaultUIUtil().onSelected(foregroundView)
           }else if (viewHolder is CartAdapter.ViewHolder)
           {
               var foregroundView = (viewHolder as CartAdapter.ViewHolder).view_foreground_cart
               ItemTouchHelper.Callback.getDefaultUIUtil().onSelected(foregroundView)
           }
        }
    }

    override fun onChildDraw(c: Canvas?, recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
       if (viewHolder is FavoriteAdapter.ViewHolder)
       {
           var foregroundView = (viewHolder as FavoriteAdapter.ViewHolder).view_foreground_favorite
           ItemTouchHelper.Callback.getDefaultUIUtil().onDraw(c,recyclerView,foregroundView,dX,dY,actionState,isCurrentlyActive)
       }else if (viewHolder is CartAdapter.ViewHolder)
       {
           var foregroundView = (viewHolder as CartAdapter.ViewHolder).view_foreground_cart
           ItemTouchHelper.Callback.getDefaultUIUtil().onDraw(c,recyclerView,foregroundView,dX,dY,actionState,isCurrentlyActive)
       }
    }

    override fun onChildDrawOver(c: Canvas?, recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        if (viewHolder is FavoriteAdapter.ViewHolder)
        {
            var foregroundView = (viewHolder as FavoriteAdapter.ViewHolder).view_foreground_favorite
            ItemTouchHelper.Callback.getDefaultUIUtil().onDrawOver(c,recyclerView,foregroundView,dX,dY,actionState,isCurrentlyActive)
        }else if (viewHolder is CartAdapter.ViewHolder)
        {
            var foregroundView = (viewHolder as CartAdapter.ViewHolder).view_foreground_cart
            ItemTouchHelper.Callback.getDefaultUIUtil().onDrawOver(c,recyclerView,foregroundView,dX,dY,actionState,isCurrentlyActive)
        }
    }
}