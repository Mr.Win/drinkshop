package com.example.vanthang.drinkshop

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import android.widget.RelativeLayout
import com.example.vanthang.drinkshop.Adapter.FavoriteAdapter
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.Common.RecyclerItemTouchHelper
import com.example.vanthang.drinkshop.Common.RecyclerItemTouchHelperListener
import com.example.vanthang.drinkshop.Database.ModelDB.Favorite
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_favorite.*
import java.lang.StringBuilder


class FavoriteActivity : AppCompatActivity(),RecyclerItemTouchHelperListener {


    lateinit var compositeDisposable: CompositeDisposable
    lateinit var favoriteAdapter:FavoriteAdapter
    var localFavorite=ArrayList<Favorite>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite)

        compositeDisposable=CompositeDisposable()
        rw_favorite.layoutManager= LinearLayoutManager(this@FavoriteActivity,LinearLayoutManager.VERTICAL,false)
        rw_favorite.hasFixedSize()

        var simpleCallBack:ItemTouchHelper.SimpleCallback=RecyclerItemTouchHelper(0,ItemTouchHelper.LEFT,this@FavoriteActivity)
        ItemTouchHelper(simpleCallBack).attachToRecyclerView(rw_favorite)
        loadFavoriteItems()


    }

    private fun loadFavoriteItems() {
        compositeDisposable.add(Common.favoriteRepository!!.getFavoriteItems()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : Consumer<List<Favorite>> {
                    override fun accept(favorites: List<Favorite>) {
                        displayFavoriteItems(favorites)
                    }
                }))
    }

    private fun displayFavoriteItems(favorites: List<Favorite>) {
        localFavorite= favorites as ArrayList<Favorite>
        favoriteAdapter=FavoriteAdapter(this@FavoriteActivity,favorites)
        rw_favorite.adapter=favoriteAdapter
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is FavoriteAdapter.ViewHolder)
        {
            var name=localFavorite.get(viewHolder.adapterPosition).name
            var deletedItem=localFavorite.get(viewHolder.adapterPosition)
            var deletedIndex=viewHolder.adapterPosition
            //Delete item from adapter
            favoriteAdapter.removeItem(deletedIndex)
            //Delete item from database
            Common.favoriteRepository!!.delete(deletedItem)
            var snackbar:Snackbar= Snackbar.make(rootLayout_favorite,StringBuilder(name).append("removed from Favorite List").toString(),Snackbar.LENGTH_SHORT)
            snackbar.setAction("UNDO",object :View.OnClickListener{
                override fun onClick(v: View?) {
                    favoriteAdapter.restoreItem(deletedItem,deletedIndex)
                    Common.favoriteRepository!!.insertFavorite(deletedItem)
                }
            })
            snackbar.setActionTextColor(Color.YELLOW)
            snackbar.show()
        }
    }
    override fun onResume() {
        super.onResume()
        loadFavoriteItems()
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onStop() {
        compositeDisposable.clear()
        super.onStop()
    }
}
