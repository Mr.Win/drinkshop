package com.example.vanthang.drinkshop.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.Interface.IItemClickListener
import com.example.vanthang.drinkshop.Model.Order
import com.example.vanthang.drinkshop.OrderDetailActivity
import com.example.vanthang.drinkshop.R
import java.lang.StringBuilder

class ShowOrderAdapter:RecyclerView.Adapter<ShowOrderAdapter.ViewHolder> {

    var context:Context?=null
    var listOrder:List<Order>?=null
    constructor(context: Context,listOrder:List<Order>)
    {
        this.context=context
        this.listOrder=listOrder
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view=LayoutInflater.from(context).inflate(R.layout.item_order_layout,null,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listOrder!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txt_order_id!!.text=StringBuilder("#").append(listOrder!![position].OrderId).toString()
        holder.txt_order_price!!.text=StringBuilder("$").append(listOrder!![position].OderPrice).toString()
        holder.txt_order_address!!.text=listOrder!![position].OderAddress
        holder.txt_order_comment!!.text=listOrder!![position].OderComment
        holder.txt_order_status!!.text=StringBuilder("Order Status: ").append(Common.convertCodeToStatus(listOrder!![position].OderStatus))
        holder.setItemClickListener(object :IItemClickListener{
            override fun onClick(v: View) {
                Common.current_Order_Detail= listOrder!!.get(position)
                context!!.startActivity(Intent(context,OrderDetailActivity::class.java))
            }
        })
    }
    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView),View.OnClickListener
    {


        private var itemClickListener:IItemClickListener?=null
        var txt_order_id:TextView?=null
        var txt_order_price:TextView?=null
        var txt_order_address:TextView?=null
        var txt_order_comment:TextView?=null
        var txt_order_status:TextView?=null
        init {
            txt_order_id=itemView.findViewById(R.id.txt_show_order_id)
            txt_order_price=itemView.findViewById(R.id.txt_show_order_price)
            txt_order_address=itemView.findViewById(R.id.txt_show_order_address)
            txt_order_comment=itemView.findViewById(R.id.txt_show_order_comment)
            txt_order_status=itemView.findViewById(R.id.txt_show_order_status)
            itemView.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            this.itemClickListener!!.onClick(v!!)
        }
        fun setItemClickListener(itemClickListener:IItemClickListener){
            this.itemClickListener=itemClickListener
        }
    }
}