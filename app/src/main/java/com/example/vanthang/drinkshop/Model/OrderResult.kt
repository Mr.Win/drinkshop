package com.example.vanthang.drinkshop.Model

class OrderResult {
    var OrderId:Long?=null
    var OrderDate:String?=null
    var OderStatus:Int?=null
    var OderPrice:Float?=null
    var OderDetail:String?=null
    var OderComment:String?=null
    var OderAddress:String?=null
    var UserPhone:String?=null
    var PaymentMethod:String?=null
}