package com.example.vanthang.drinkshop.Retrofit

import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

class RetrofitScalarsClient {
    companion object {
        private var retrofit: Retrofit?=null
        fun getScalarsClient(baseUrl:String):Retrofit{
            if (retrofit==null)
            {
                retrofit=Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .build()
            }
            return retrofit!!
        }
    }
}