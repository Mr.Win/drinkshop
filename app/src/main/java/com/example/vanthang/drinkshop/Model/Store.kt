package com.example.vanthang.drinkshop.Model

class Store {
    var id:String?=null
    var name:String?=null
    var lat:String?=null
    var lng:String?=null
    var distance_in_km:String?=null
}