package com.example.vanthang.drinkshop.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.Database.ModelDB.Cart
import com.example.vanthang.drinkshop.R
import com.squareup.picasso.Picasso
import java.lang.StringBuilder

class CartAdapter : RecyclerView.Adapter<CartAdapter.ViewHolder> {

    private var context:Context?=null
    private var cartList:ArrayList<Cart>?=null
    constructor(context: Context,cartList: ArrayList<Cart>)
    {
        this.context=context
        this.cartList=cartList
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view=LayoutInflater.from(context).inflate(R.layout.item_cart_layout,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return cartList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.with(context)
                .load(cartList!![position].link)
                .into(holder.img_order)
        holder.txt_order_name!!.text=StringBuilder(cartList!![position].name)
                .append(if (cartList!![position].size==0) " Size M" else " Size L")
                .append(" x")
                .append(cartList!![position].amount).toString()
        holder.txt_order_sugar_ice!!.text=StringBuilder("Sugar: ")
                .append(cartList!![position].sugar.toString())
                .append("%")
                .append("\n")
                .append("Ice: ")
                .append(cartList!![position].ice.toString())
                .append("%").toString()
        holder.txt_order_price!!.text=StringBuilder("$").append(cartList!![position].price.toString())
        holder.btn_number_order!!.number=cartList!![position].amount.toString()

        //Get price of one cup with all options
        var priceonecup= cartList!![position].price!! / cartList!![position].amount!!
        //Auto save item when user change amount
        holder!!.btn_number_order!!.setOnValueChangeListener(object :ElegantNumberButton.OnValueChangeListener{
            override fun onValueChange(view: ElegantNumberButton?, oldValue: Int, newValue: Int) {
                var cart:Cart=cartList!!.get(position)
                cart.amount=newValue
                cart.price=priceonecup*newValue
                Common.cartRepository!!.updateToCart(cart)

                holder.txt_order_price!!.text=StringBuilder("$").append(cartList!![position].price.toString())
            }
        })
    }

    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        var img_order:ImageView?=null
        var txt_order_name:TextView?=null
        var txt_order_sugar_ice:TextView?=null
        var txt_order_price:TextView?=null
        var btn_number_order:ElegantNumberButton?=null
        var view_background_cart:RelativeLayout?=null
        var view_foreground_cart:LinearLayout?=null
        init {
            img_order=itemView.findViewById(R.id.img_order)
            txt_order_name=itemView.findViewById(R.id.txt_order_name)
            txt_order_sugar_ice=itemView.findViewById(R.id.txt_order_sugar_ice)
            txt_order_price=itemView.findViewById(R.id.txt_order_price)
            btn_number_order=itemView.findViewById(R.id.btn_number_order)
            view_background_cart=itemView.findViewById(R.id.view_background_cart)
            view_foreground_cart=itemView.findViewById(R.id.view_foreground_cart)
        }
    }
    fun removeItem(position: Int)
    {
        cartList!!.removeAt(position)
        notifyItemRemoved(position)
    }
    fun restoreItem(item:Cart,position: Int)
    {
        cartList!!.add(position,item)
        notifyItemInserted(position)
    }
}