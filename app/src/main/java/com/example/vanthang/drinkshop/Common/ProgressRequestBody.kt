package com.example.vanthang.drinkshop.Common


import android.os.Handler
import android.os.Looper
import okhttp3.MediaType
import okhttp3.RequestBody
import okio.BufferedSink
import java.io.File
import java.io.FileInputStream
import java.io.RandomAccessFile


class ProgressRequestBody :RequestBody {
    private var file:File?=null
    var listener:UploadCallBack?=null


    companion object {
        private const val  DEFAULT_BUFFER_SIZE=4096
    }
    constructor(file: File,listener:UploadCallBack)
    {
        this.file=file
        this.listener=listener
    }

    override fun contentLength(): Long {
        return file!!.length()
    }

    override fun contentType(): MediaType? {
        return MediaType.parse("image/*")
    }

    override fun writeTo(sink: BufferedSink) {
        var fileLength=file!!.length()
        var buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        var fileInputStream=FileInputStream(file)
        var uploaded:Long =0
        try {
            var read:Int?=null
            var handler= Handler(Looper.getMainLooper())
            read=fileInputStream.read(buffer)
            while (read != -1) {
                handler.post(ProgressUpdater(uploaded,fileLength))
                uploaded +=read!!
                sink.write(buffer,0,read)
                read=fileInputStream.read(buffer)
            }
        }finally {
            fileInputStream.close()
        }

    }

    inner class ProgressUpdater: Runnable {

        private var uploaded:Long?=null
        private var fileLength:Long?=null
        constructor(uploaded:Long,fileLength:Long)
        {
            this.uploaded=uploaded
            this.fileLength=fileLength
        }
        override fun run() {
            listener!!.onProgressUpdate(100*uploaded!!/fileLength!!)
        }

    }
}