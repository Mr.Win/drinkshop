package com.example.vanthang.drinkshop.Database.DataSource

import com.example.vanthang.drinkshop.Database.ModelDB.Cart
import io.reactivex.Flowable

interface ICartDataSource {
    fun getCartItems(): Flowable<List<Cart>>
    fun getCartItemById(cartItemId:Int): Flowable<List<Cart>>
    fun countCartItems():Int
    fun sumPrice():Float
    fun emptyCart()
    fun insertToCart(vararg carts: Cart)
    fun updateToCart(vararg carts: Cart)
    fun deleteCartItem(carts: Cart)
}