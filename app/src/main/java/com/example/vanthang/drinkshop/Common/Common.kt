package com.example.vanthang.drinkshop.Common

import com.example.vanthang.drinkshop.Database.DataSource.CartRepository
import com.example.vanthang.drinkshop.Database.DataSource.FavoriteRepository
import com.example.vanthang.drinkshop.Database.Local.ANTRoomDatabase
import com.example.vanthang.drinkshop.Model.Category
import com.example.vanthang.drinkshop.Model.Drinks
import com.example.vanthang.drinkshop.Model.Order
import com.example.vanthang.drinkshop.Model.User
import com.example.vanthang.drinkshop.Retrofit.*

class Common {
    companion object {
        const val BASE_URL:String="http://10.0.128.104/drinkshop/" //10.0.128.104
        const val API_TOKEN_URL:String="http://10.0.128.104/drinkshop/braintree/main.php"
        const val FCM_API:String="https://fcm.googleapis.com/"
        internal var currentUser:User?=null
        internal var currentCategory:Category?=null
        internal var toppingList:List<Drinks>?=null
        internal var current_Order_Detail:Order?=null
        internal val TOPPING_MENU_ID ="6"
        internal var toppingPrice:Double=0.0
        internal var toppingAdded:ArrayList<String> = ArrayList()
        //hold field
        internal var sizeofCup=-1   //no choice (error),0 M,1 L
        internal var sugar=-1   //no choice(error)
        internal var ice=-1 //no choice(error)

        //Database
        var ANTRoomDatabase: ANTRoomDatabase?=null
        var cartRepository:CartRepository?=null
        var favoriteRepository:FavoriteRepository?=null

        fun getFCMService():IFCMService
        {
            return FCMClient.getClient(FCM_API).create(IFCMService::class.java)
        }
        fun getAPI():IDrinkShopAPI
        {
            return RetrofitClient.getClient(BASE_URL).create(IDrinkShopAPI::class.java)
        }
        fun getScalarsAPI():IDrinkShopAPI
        {
            return RetrofitScalarsClient.getScalarsClient(BASE_URL).create(IDrinkShopAPI::class.java)
        }

        fun convertCodeToStatus(oderStatus: Int?): Any {
            when(oderStatus)
            {
                0   -> return "Placed"
                1   -> return "Processing"
                2   -> return "Shipping"
                3   -> return "Shipped"
                -1  -> return "Cancelled"
                else -> return "Order Error"
            }
        }
    }
}