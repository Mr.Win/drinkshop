package com.example.vanthang.drinkshop.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.DrinkActivity
import com.example.vanthang.drinkshop.Interface.IItemClickListener
import com.example.vanthang.drinkshop.Model.Category
import com.example.vanthang.drinkshop.R
import com.squareup.picasso.Picasso

class CategoryAdapter(var context: Context, var categories:List<Category>):RecyclerView.Adapter<CategoryAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view=LayoutInflater.from(context).inflate(R.layout.item_category,null)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return categories!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.with(context)
                .load(categories!![position].Link)
                .into(holder.img_category)
        holder.txt_category!!.text=categories!![position].Name
        holder.setItemClickListener(object :IItemClickListener{
            override fun onClick(v: View) {
                Common.currentCategory=categories.get(position)

                //Start new Activity Item Drink
                context.startActivity(Intent(context,DrinkActivity::class.java))
            }

        })
    }


    class ViewHolder(item: View):RecyclerView.ViewHolder(item),View.OnClickListener
    {
        private var itemClickListener:IItemClickListener?=null
        internal var img_category:ImageView?=null
        internal var txt_category:TextView?=null

        init {
            item.setOnClickListener(this)
            img_category=item.findViewById(R.id.img_item_category)
            txt_category=item.findViewById(R.id.txt_item_category)
        }
        override fun onClick(v: View?) {
            this.itemClickListener!!.onClick(v!!)
        }
        fun setItemClickListener(iItemClickListener: IItemClickListener)
        {
            this.itemClickListener=iItemClickListener
        }
    }
}

