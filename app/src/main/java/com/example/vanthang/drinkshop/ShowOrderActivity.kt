package com.example.vanthang.drinkshop

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.widget.Toast
import com.example.vanthang.drinkshop.Adapter.ShowOrderAdapter
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.Model.Order
import com.example.vanthang.drinkshop.Retrofit.IDrinkShopAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_show_order.*

class ShowOrderActivity : AppCompatActivity() {

    lateinit var mService:IDrinkShopAPI
    lateinit var showOrderAdapter:ShowOrderAdapter

    //RxJava
    var compositedisposable:CompositeDisposable= CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_order)

        mService=Common.getAPI()
//        disableShiftMode(nav_bottom_filter_order)
        rw_show_orders.layoutManager=LinearLayoutManager(this@ShowOrderActivity,LinearLayoutManager.VERTICAL,false)
        rw_show_orders.hasFixedSize()

        nav_bottom_filter_order.setOnNavigationItemSelectedListener (object :BottomNavigationView.OnNavigationItemSelectedListener{
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when(item.itemId)
                {
                    R.id.order_new -> loadOrderStatus("0")
                    R.id.order_cancel -> loadOrderStatus("-1")
                    R.id.order_processing -> loadOrderStatus("1")
                    R.id.order_shipping -> loadOrderStatus("2")
                    R.id.order_done -> loadOrderStatus("3")
                }
                return true
            }
        })
        loadOrderStatus("0")
    }
    //disable shiftMode
//    @SuppressLint("RestrictedApi")
//    fun disableShiftMode(view: BottomNavigationView) {
//        val menuView = view.getChildAt(0) as BottomNavigationMenuView
//        try {
//            val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
//            shiftingMode.isAccessible = true
//            shiftingMode.setBoolean(menuView, false)
//            shiftingMode.isAccessible = false
//            for (i in 0 until menuView.childCount) {
//                val item = menuView.getChildAt(i) as BottomNavigationItemView
//                item.setShiftingMode(false)
//                // set once again checked value, so view will be updated
//                item.setChecked(item.itemData.isChecked)
//            }
//        } catch (e: NoSuchFieldException) {
//            //Timber.e(e, "Unable to get shift mode field");
//        } catch (e: IllegalAccessException) {
//            //Timber.e(e, "Unable to change value of shift mode");
//        }
//
//    }
    private fun loadOrderStatus(statusCode:String) {
        if (Common.currentUser !=null)
        {
            compositedisposable.add(mService.getOrderStatus(Common.currentUser!!.phone!!,statusCode)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object :Consumer<List<Order>>{
                        override fun accept(listOrder: List<Order>?) {
                            displayShowOrder(listOrder)
                        }
                    }))
        }else
        {
            Toast.makeText(this@ShowOrderActivity,"Please login again",Toast.LENGTH_SHORT).show()
            finish()
        }
    }


    private fun displayShowOrder(listOrder: List<Order>?) {
        showOrderAdapter= ShowOrderAdapter(this@ShowOrderActivity,listOrder!!)
        rw_show_orders.adapter=showOrderAdapter
    }


    override fun onResume() {
        super.onResume()
        loadOrderStatus("0")
    }

    override fun onStop() {
        compositedisposable.clear()
        super.onStop()
    }

    override fun onDestroy() {
        compositedisposable.clear()
        super.onDestroy()
    }
}
