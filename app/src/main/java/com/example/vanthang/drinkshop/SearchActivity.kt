package com.example.vanthang.drinkshop

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.text.Editable
import android.text.TextWatcher
import com.example.vanthang.drinkshop.Adapter.DrinkAdapter
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.Model.Drinks
import com.example.vanthang.drinkshop.Retrofit.IDrinkShopAPI
import com.mancj.materialsearchbar.MaterialSearchBar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_search.*


class SearchActivity : AppCompatActivity() {

    lateinit var mService:IDrinkShopAPI
    lateinit var searchAdapter:DrinkAdapter
    lateinit var listDrinkadapter: DrinkAdapter
   lateinit var suggestList:List<String>
    var localDataSource:ArrayList<Drinks>?=null

    //rxJava
    var compositeDisposable: CompositeDisposable = CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        //init service
        mService=Common.getAPI()

        rw_searchbar.layoutManager=GridLayoutManager(this,2)

        searchBar.setHint("Enter your drink")

        loadAllDrinks()

        searchBar.setCardViewElevation(10)
        searchBar.addTextChangeListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var suggest=ArrayList<String>()
                for (search in suggestList!!)
                {
                    if (search.toLowerCase().contains(searchBar.text.toLowerCase()))
                        suggest.add(search)
                }
                searchBar.lastSuggestions=suggest
            }
        })
        searchBar.setOnSearchActionListener(object :MaterialSearchBar.OnSearchActionListener{
            override fun onButtonClicked(buttonCode: Int) {

            }

            override fun onSearchStateChanged(enabled: Boolean) {
                if (!enabled)
                    rw_searchbar.adapter=listDrinkadapter //Restore full list of drinks
            }

            override fun onSearchConfirmed(text: CharSequence?) {
                startSearch(text!!)
            }
        })

    }

    private fun startSearch(text:CharSequence)
    {
        var result=ArrayList<Drinks>()
        for (drink in localDataSource!!)
        {
            if (drink.Name!!.contains(text))
                result.add(drink)
            searchAdapter=DrinkAdapter(this@SearchActivity,result!!)
            rw_searchbar.adapter=searchAdapter
        }
    }
    private fun loadAllDrinks() {
        compositeDisposable.add(mService.getAllDrinks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Consumer<List<Drinks>> {
                    override fun accept(drinks: List<Drinks>) {
                        displayListDrinks(drinks)
                        buildSuggestList(drinks)
                    }
                }))

    }

    override fun onStop() {
        compositeDisposable.clear()
        super.onStop()
    }

    private fun buildSuggestList(drinks: List<Drinks>) {
        suggestList= ArrayList<String>()
        for (drink in drinks)
            (suggestList as ArrayList<String>).add(drink.Name!!)
        searchBar.lastSuggestions=suggestList
    }

    private fun displayListDrinks(drinks: List<Drinks>) {
        localDataSource= drinks as ArrayList<Drinks>
        listDrinkadapter=DrinkAdapter(this@SearchActivity,drinks)
        rw_searchbar.adapter=listDrinkadapter
    }
}
