package com.example.vanthang.drinkshop.Database.ModelDB

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity(tableName = "Favorite")
class Favorite {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: String?=null

    @ColumnInfo(name = "name")
    var name: String? = null

    @ColumnInfo(name = "link")
    var link: String? = null

    @ColumnInfo(name = "price")
    var price: String? = null

    @ColumnInfo(name = "menuId")
    var menuId: String? = null
}