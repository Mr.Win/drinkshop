package com.example.vanthang.drinkshop

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.Model.CheckUserResponse
import com.example.vanthang.drinkshop.Model.User
import com.example.vanthang.drinkshop.Retrofit.IDrinkShopAPI
import com.facebook.accountkit.*
import com.facebook.accountkit.ui.AccountKitActivity
import com.facebook.accountkit.ui.AccountKitConfiguration
import com.facebook.accountkit.ui.LoginType
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.rengwuxian.materialedittext.MaterialEditText
import com.szagurskii.patternedtextwatcher.PatternedTextWatcher
import dmax.dialog.SpotsDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception


class MainActivity : AppCompatActivity() {

    lateinit var btn_continue: Button
    internal val REQUEST_CODE = 1000
    internal val REQUEST_PERMISION=1001
    lateinit var mService: IDrinkShopAPI

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode)
        {
            REQUEST_PERMISION->{
                if (grantResults.isNotEmpty() && grantResults[0]==PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this,"Permission granted",Toast.LENGTH_SHORT).show()
                else
                    Toast.makeText(this,"Permission denied",Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (ActivityCompat.checkSelfPermission(this@MainActivity,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this@MainActivity,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),REQUEST_PERMISION)
        }
        mService = Common.getAPI()
        btn_continue = findViewById(R.id.btn_continue) as Button

        //event btn
        btn_continue.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                startLoginPage(LoginType.PHONE)
            }

        })
        //check session
        if (AccountKit.getCurrentAccessToken() !=null)
        {
            //Auto login
            var alertDialog: AlertDialog = SpotsDialog.Builder().setContext(this@MainActivity).build()
            alertDialog.show()
            alertDialog.setMessage("Please waiting...")
            AccountKit.getCurrentAccount(object : AccountKitCallback<Account> {
                override fun onSuccess(account: Account?) {
                    mService.checkUserExists(account!!.phoneNumber.toString())
                            .enqueue(object : retrofit2.Callback<CheckUserResponse> {
                                override fun onFailure(call: Call<CheckUserResponse>, t: Throwable) {
                                    alertDialog.dismiss()
                                }

                                override fun onResponse(call: Call<CheckUserResponse>, response: Response<CheckUserResponse>) {
                                    var checkUserResponse = response?.body()
                                    if (checkUserResponse!!.exists) {
                                        //fetch user information
                                        mService.getUserInformation(account!!.phoneNumber.toString())
                                                .enqueue(object :Callback<User>{
                                                    override fun onFailure(call: Call<User>, t: Throwable) {
                                                        Toast.makeText(this@MainActivity,""+t.message,Toast.LENGTH_SHORT).show()
                                                    }

                                                    override fun onResponse(call: Call<User>, response: Response<User>) {
                                                        //if user already exists ,then start new Activity
                                                        Common.currentUser=response.body()

                                                        //update token
                                                        updateTokenToServer()
                                                        alertDialog.dismiss()
                                                        startActivity(Intent(this@MainActivity,HomeActivity::class.java))
                                                        finish()
                                                    }

                                                })
                                    } else {
                                        //need register
                                        alertDialog.dismiss()
                                        showRegisterDialog(account!!.phoneNumber.toString())
                                    }
                                }

                            })
                }

                override fun onError(p0: AccountKitError?) {

                }

            })
        }
    }

    private fun startLoginPage(phone: LoginType) {
        var intent: Intent = Intent(this, AccountKitActivity::class.java)
        var builder: AccountKitConfiguration.AccountKitConfigurationBuilder =
                AccountKitConfiguration.AccountKitConfigurationBuilder(phone,
                        AccountKitActivity.ResponseType.TOKEN)
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, builder.build())
        startActivityForResult(intent, REQUEST_CODE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE) {
            var result: AccountKitLoginResult = data!!.getParcelableExtra(AccountKitLoginResult.RESULT_KEY)
            if (result.error != null) {
                Toast.makeText(this, "" + result!!.error!!.errorType.message, Toast.LENGTH_SHORT).show()
            } else if (result.wasCancelled()) {
                Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show()
            } else {
                if (result.accessToken != null) {
                    var alertDialog: AlertDialog = SpotsDialog.Builder().setContext(this@MainActivity).build()
                    alertDialog.show()
                    alertDialog.setMessage("Please waiting...")

                    //Get user phone and check exists on service
                    AccountKit.getCurrentAccount(object : AccountKitCallback<Account> {
                        override fun onSuccess(account: Account?) {
                            mService.checkUserExists(account!!.phoneNumber.toString())
                                    .enqueue(object : retrofit2.Callback<CheckUserResponse> {
                                        override fun onFailure(call: Call<CheckUserResponse>, t: Throwable) {
                                            alertDialog.dismiss()
                                        }

                                        override fun onResponse(call: Call<CheckUserResponse>, response: Response<CheckUserResponse>) {
                                            var checkUserResponse = response?.body()
                                            if (checkUserResponse!!.exists) {
                                                //fetch user information
                                                mService.getUserInformation(account!!.phoneNumber.toString())
                                                        .enqueue(object :Callback<User>{
                                                            override fun onFailure(call: Call<User>, t: Throwable) {
                                                                Toast.makeText(this@MainActivity,""+t.message,Toast.LENGTH_SHORT).show()
                                                            }

                                                            override fun onResponse(call: Call<User>, response: Response<User>) {
                                                                //if user already exists ,then start new Activity
                                                                alertDialog.dismiss()
                                                                Common.currentUser=response.body()
                                                                //update token
                                                                updateTokenToServer()
                                                                startActivity(Intent(this@MainActivity,HomeActivity::class.java))
                                                                finish()
                                                            }

                                                        })
                                            } else {
                                                //need register
                                                alertDialog.dismiss()
                                                showRegisterDialog(account!!.phoneNumber.toString())
                                            }
                                        }

                                    })
                        }

                        override fun onError(p0: AccountKitError?) {

                        }

                    })
                }
            }
        }
    }

    private fun showRegisterDialog(phone: String) {
        var builder_register: AlertDialog.Builder = AlertDialog.Builder(this@MainActivity)
        builder_register.setTitle("REGISTER")
        builder_register.setCancelable(false)
        var dialog: AlertDialog? = null

        var layoutInflater: LayoutInflater = this.layoutInflater
        var register_layout: View = layoutInflater.inflate(R.layout.register_layout, null)

        var edt_name = register_layout.findViewById(R.id.edt_register_name) as MaterialEditText
        var edt_birthdate = register_layout.findViewById(R.id.edt_register_birthdate) as MaterialEditText
        var edt_address = register_layout.findViewById(R.id.edt_register_address) as MaterialEditText
        var btn_register = register_layout.findViewById(R.id.btn_register) as Button

        edt_birthdate.addTextChangedListener(PatternedTextWatcher("####-##-##"))

        builder_register.setView(register_layout)
        dialog = builder_register.create()

        //event
        btn_register.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                dialog.dismiss()
                if (TextUtils.isEmpty(edt_name.text.toString())) {
                    Toast.makeText(this@MainActivity, "Please enter your name", Toast.LENGTH_SHORT).show()
                    return
                }
                if (TextUtils.isEmpty(edt_birthdate.text.toString())) {
                    Toast.makeText(this@MainActivity, "Please enter your birthdate", Toast.LENGTH_SHORT).show()
                    return
                }
                if (TextUtils.isEmpty(edt_address.text.toString())) {
                    Toast.makeText(this@MainActivity, "Please enter your address", Toast.LENGTH_SHORT).show()
                    return
                }
                val waitingDialog: AlertDialog = SpotsDialog.Builder().setContext(this@MainActivity).build()
                waitingDialog.show()
                waitingDialog.setMessage("Please waiting...")

                mService.registerNewUser(phone,
                        edt_name.text.toString(),
                        edt_birthdate.text.toString(),
                        edt_address.text.toString())
                        .enqueue(object : retrofit2.Callback<User> {
                            override fun onFailure(call: Call<User>, t: Throwable) {
                                waitingDialog.dismiss()
                            }

                            override fun onResponse(call: Call<User>, response: Response<User>) {
                                waitingDialog.dismiss()
                                var user = response.body()
                                if (TextUtils.isEmpty(user!!.error_msg)) {
                                    Toast.makeText(this@MainActivity, "User register successfully", Toast.LENGTH_SHORT).show()
                                    //start new Activity
                                    Common.currentUser=user
                                    //update token
                                    updateTokenToServer()
                                    startActivity(Intent(this@MainActivity,HomeActivity::class.java))
                                    finish()
                                }
                            }

                        })
            }

        })
        dialog.show()
    }

    // exit application when click BACK button
    var isBackButtonClicked=false

    override fun onBackPressed() {
        if(isBackButtonClicked)
        {
            super.onBackPressed()
            return
        }
        this.isBackButtonClicked=true
        Toast.makeText(this@MainActivity,"Please click BACK again to exit",Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        isBackButtonClicked=false
        super.onResume()
    }
    private fun updateTokenToServer() {
       FirebaseInstanceId.getInstance()
               .instanceId
               .addOnSuccessListener(object :OnSuccessListener<InstanceIdResult>{
                   override fun onSuccess(p0: InstanceIdResult?) {
                       var mService=Common.getAPI()
                       mService.updateToken(Common.currentUser!!.phone!!,p0!!.token,"0")
                               .enqueue(object : Callback<String>{
                                   override fun onFailure(call: Call<String>, t: Throwable) {
                                       Log.d("DEBUG",t.message)
                                   }

                                   override fun onResponse(call: Call<String>, response: Response<String>) {
                                       Log.d("DEBUG",response.toString())
                                   }
                               })
                   }
               })
               .addOnFailureListener(object :OnFailureListener{
                   override fun onFailure(p0: Exception) {
                       Toast.makeText(this@MainActivity,p0.message.toString(),Toast.LENGTH_SHORT).show()
                   }
               })
    }
}
