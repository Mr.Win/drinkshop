package com.example.vanthang.drinkshop


import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Toast
import com.braintreepayments.api.dropin.DropInActivity
import com.braintreepayments.api.dropin.DropInRequest
import com.braintreepayments.api.dropin.DropInResult
import com.braintreepayments.api.models.PaymentMethodNonce
import com.example.vanthang.drinkshop.Adapter.CartAdapter
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.Common.RecyclerItemTouchHelper
import com.example.vanthang.drinkshop.Common.RecyclerItemTouchHelperListener
import com.example.vanthang.drinkshop.Database.ModelDB.Cart
import com.example.vanthang.drinkshop.Model.DataMessage
import com.example.vanthang.drinkshop.Model.MyResponse
import com.example.vanthang.drinkshop.Model.OrderResult
import com.example.vanthang.drinkshop.Model.Token
import com.example.vanthang.drinkshop.Retrofit.IDrinkShopAPI
import com.example.vanthang.drinkshop.Retrofit.IFCMService
import com.google.gson.Gson
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.TextHttpResponseHandler
import cz.msebera.android.httpclient.Header
import dmax.dialog.SpotsDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.submit_order_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CartActivity : AppCompatActivity() ,RecyclerItemTouchHelperListener{


    lateinit var cartadapter:CartAdapter
    lateinit var compositeDisposable:CompositeDisposable
    var localCart=ArrayList<Cart>()
    lateinit var  mService:IDrinkShopAPI
    lateinit var mServiceScalars:IDrinkShopAPI
    //global string
    var token:String?=null
    var amount:String?=null
    var orderAddress:String?=null
    var orderComment:String?=null
    lateinit var params:HashMap<String,String>
    val PAYMENT_REQUEST_CODE=5555

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        mService =Common.getAPI()
        mServiceScalars =Common.getScalarsAPI()
        compositeDisposable=CompositeDisposable()

        rw_order.layoutManager=LinearLayoutManager(this@CartActivity,LinearLayoutManager.VERTICAL,false)
        rw_order.hasFixedSize()

        btn_place_order.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                placeOrder()
            }
        })

        var simpleCallBack: ItemTouchHelper.SimpleCallback= RecyclerItemTouchHelper(0,ItemTouchHelper.LEFT,this@CartActivity)
        ItemTouchHelper(simpleCallBack).attachToRecyclerView(rw_order)
        loadCartItems()

        loadToken()
    }

    private fun loadToken() {
        var waitingDialog: AlertDialog = SpotsDialog.Builder().setContext(this@CartActivity).build()
        waitingDialog.show()
        waitingDialog.setMessage("Please wait...")

        var client=AsyncHttpClient()
        client.get(Common.API_TOKEN_URL,object :TextHttpResponseHandler(){
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseString: String?) {
                waitingDialog.dismiss()
                token=responseString
                if (Common.cartRepository!!.countCartItems() >0)
                {
                    btn_place_order.isEnabled=true
                }
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseString: String?, throwable: Throwable?) {
                waitingDialog.dismiss()
                btn_place_order.isEnabled=false
                Toast.makeText(this@CartActivity, throwable!!.message,Toast.LENGTH_SHORT).show()

            }
        })
    }

    private fun placeOrder() {
        var builder:AlertDialog.Builder=AlertDialog.Builder(this@CartActivity)
        builder.setTitle("Submit Order")

        var submit_order_layout:View=LayoutInflater.from(this@CartActivity).inflate(R.layout.submit_order_layout,null,false)

        var edt_comment_ordersubmit=submit_order_layout.findViewById<EditText>(R.id.edt_comment_submitorder)
        var rd_user_address=submit_order_layout.findViewById<RadioButton>(R.id.rd_user_address)
        var rd_other_address=submit_order_layout.findViewById<RadioButton>(R.id.rd_other_address)
        var edt_other_address=submit_order_layout.findViewById<EditText>(R.id.edt_other_address)
        var rd_credit_card=submit_order_layout.findViewById<RadioButton>(R.id.rd_credit_card)
        var rd_cod=submit_order_layout.findViewById<RadioButton>(R.id.rd_cod)

        rd_user_address.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                    edt_other_address.isEnabled=false
            }
        })
        rd_other_address.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                    edt_other_address.isEnabled=true
            }
        })
        builder.setNegativeButton("CANCEL",object :DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                dialog!!.dismiss()
            }
        }).setPositiveButton("SUBMIT",object :DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                if (rd_credit_card.isChecked) {
                    orderComment = edt_comment_ordersubmit.text.toString()
                    if (rd_user_address.isChecked)
                        orderAddress = Common.currentUser!!.address!!
                    else if (rd_other_address.isChecked)
                        orderAddress = edt_other_address.text.toString()

                    //Payment
                    var dropInRequest = DropInRequest().clientToken(token)
                    startActivityForResult(dropInRequest.getIntent(this@CartActivity), PAYMENT_REQUEST_CODE)
                }else if (rd_cod.isChecked)
                {
                    orderComment = edt_comment_ordersubmit.text.toString()
                    if (rd_user_address.isChecked)
                        orderAddress = Common.currentUser!!.address!!
                    else if (rd_other_address.isChecked)
                        orderAddress = edt_other_address.text.toString()
                    //payment
                    compositeDisposable.add(Common.cartRepository!!.getCartItems()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(object :Consumer<List<Cart>>{
                                override fun accept(carts: List<Cart>?) {
                                    if (!TextUtils.isEmpty(orderAddress))
                                        sendOrderToSerVer(Common.cartRepository!!.sumPrice(),
                                                carts,
                                                orderComment!!,
                                                orderAddress,"COD")
                                    else
                                        Toast.makeText(this@CartActivity,"Order address can't null",Toast.LENGTH_SHORT).show()
                                }
                            }))
                }
            }
        })
        builder.setView(submit_order_layout)
        builder.show()


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode==PAYMENT_REQUEST_CODE)
        {
            if (resultCode== Activity.RESULT_OK)
            {
                var result:DropInResult=data!!.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT)
                var nonce:PaymentMethodNonce= result.paymentMethodNonce!!
                var strNonce:String=nonce.nonce

                if (Common.cartRepository!!.sumPrice() >0 )
                {
                    amount= Common.cartRepository!!.sumPrice().toString()
                    params=HashMap()
                    params.put("amount", amount!!)
                    params.put("nonce",strNonce)

                    sendPayment()
                }else{
                    Toast.makeText(this@CartActivity,"Payment amount is 0",Toast.LENGTH_SHORT).show()

                }
            }else if (resultCode== Activity.RESULT_CANCELED)
            {
                Toast.makeText(this@CartActivity,"Payment cancelled",Toast.LENGTH_SHORT).show()
            }else
            {
                var error:Exception= data!!.getSerializableExtra(DropInActivity.EXTRA_ERROR) as Exception
                Log.e("ANT ERROR",error.message)
            }
        }
    }

    private fun sendPayment() {
        mServiceScalars.payment(params.get("nonce")!!, params.get("amount")!!)
                .enqueue(object :retrofit2.Callback<String>{
                    override fun onFailure(call: Call<String>, t: Throwable) {
                        Log.d("ANT_ERROR",t.message)
                    }

                    override fun onResponse(call: Call<String>, response: Response<String>) {
                        if (response.body().toString().contains("Successful"))
                        {
                            Toast.makeText(this@CartActivity,"Transaction successful",Toast.LENGTH_SHORT).show()

                            //submit order
                            compositeDisposable.add(Common.cartRepository!!.getCartItems()
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(object :Consumer<List<Cart>>{
                                        override fun accept(carts: List<Cart>?) {
                                            if (!TextUtils.isEmpty(orderAddress))
                                                sendOrderToSerVer(Common.cartRepository!!.sumPrice(),
                                                        carts,
                                                        orderComment!!,
                                                        orderAddress,"Braintree")
                                            else
                                                Toast.makeText(this@CartActivity,"Order address can't null",Toast.LENGTH_SHORT).show()
                                        }
                                    }))
                        }

                        else
                            Toast.makeText(this@CartActivity,"Transaction failed",Toast.LENGTH_SHORT).show()
                        Log.d("ANT_INFOR",response.body().toString())
                    }
                })
    }

    private fun sendOrderToSerVer(sumPrice: Float, carts: List<Cart>?, orderComment: String, orderAddress: String?,paymentMethod:String) {
        if (carts!!.size >0)
        {
            var orderDetail= Gson().toJson(carts)
            mService.submitOrder(Common.currentUser!!.phone!!,orderDetail,orderComment,orderAddress!!,sumPrice,paymentMethod)
                    .enqueue(object :retrofit2.Callback<OrderResult>{
                        override fun onFailure(call: Call<OrderResult>, t: Throwable) {
                            Toast.makeText(this@CartActivity,t.message.toString(),Toast.LENGTH_SHORT).show()
                        }

                        override fun onResponse(call: Call<OrderResult>, response: Response<OrderResult>) {
                            sendNotificationToServer(response.body()!!)
                        }
                    })
        }
    }

    private fun sendNotificationToServer(orderResult:OrderResult) {
        mService.getToken("server_app_01","1")
                .enqueue(object :Callback<Token>{
                    override fun onFailure(call: Call<Token>, t: Throwable) {
                        Toast.makeText(this@CartActivity,t.message.toString(),Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<Token>, response: Response<Token>) {
                        //when we have token , we will send notification to this token
                        var contentSend:HashMap<String,String> = HashMap()
                        contentSend.put("title","ANT")
                        contentSend.put("message","You have new order "+orderResult.OrderId)
                        var dataMessage= DataMessage()
                        if (response.body()!!.Token !=null)
                            dataMessage.to= response.body()!!.Token
                        dataMessage.data=contentSend

                        var ifcmService: IFCMService =Common.getFCMService()
                        ifcmService.sendNotification(dataMessage)
                                .enqueue(object :Callback<MyResponse>{
                                    override fun onFailure(call: Call<MyResponse>, t: Throwable) {
                                        Toast.makeText(this@CartActivity,t.message.toString(),Toast.LENGTH_SHORT).show()
                                    }

                                    override fun onResponse(call: Call<MyResponse>, response: Response<MyResponse>) {
                                        if (response.code()==200)
                                        {
                                            if (response.body()!!.success==1)
                                            {
                                                Toast.makeText(this@CartActivity,"Thank you, Order place",Toast.LENGTH_SHORT).show()

                                                //Clear Cart
                                                Common.cartRepository!!.emptyCart()
                                                btn_place_order.isEnabled=false
                                                finish()
                                            }else
                                            {
                                                Toast.makeText(this@CartActivity,"Send notification failed",Toast.LENGTH_SHORT).show()
                                            }
                                        }
                                    }
                                })
                    }
                })
    }


    private fun loadCartItems() {
        compositeDisposable.add(Common.cartRepository!!.getCartItems()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object :Consumer<List<Cart>>{
                    override fun accept(carts: List<Cart>?) {
                        displayCartItem(carts)
                    }
                }))

    }

    private fun displayCartItem(carts: List<Cart>?) {
        localCart=carts as ArrayList<Cart>
        cartadapter=CartAdapter(this@CartActivity,carts!!)
        rw_order.adapter=cartadapter
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is CartAdapter.ViewHolder)
        {
            var name=localCart.get(viewHolder.adapterPosition).name
            var deletedItem=localCart.get(viewHolder.adapterPosition)
            var deletedIndex=viewHolder.adapterPosition
            //Delete item from adapter
            cartadapter.removeItem(deletedIndex)
            //Delete item from database
            Common.cartRepository!!.deleteCartItem(deletedItem)
            var snackbar: Snackbar = Snackbar.make(rootLayout_cart,StringBuilder(name).append("removed from Cart List").toString(),Snackbar.LENGTH_SHORT)
            snackbar.setAction("UNDO",object : View.OnClickListener{
                override fun onClick(v: View?) {
                    cartadapter.restoreItem(deletedItem,deletedIndex)
                    Common.cartRepository!!.insertToCart(deletedItem)
                }
            })
            snackbar.setActionTextColor(Color.YELLOW)
            snackbar.show()
        }
    }
    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onStop() {
        compositeDisposable.clear()
        super.onStop()
    }
    
    override fun onResume() {
        super.onResume()
        loadCartItems()
    }
}
