package com.example.vanthang.drinkshop.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.Model.Drinks
import com.example.vanthang.drinkshop.R
import kotlinx.android.synthetic.main.multi_check_layout.view.*

class MultiChoiceAdapter(var context: Context, var optionList:List<Drinks>) :RecyclerView.Adapter<MultiChoiceAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var itemView=LayoutInflater.from(context).inflate(R.layout.multi_check_layout,null)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return optionList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.cb_topping!!.text=optionList[position].Name
        holder.cb_topping!!.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked)
                {
                    Common.toppingAdded.add(buttonView!!.text.toString())
                    Common.toppingPrice += (optionList[position].Price)!!.toDouble()
                }else
                {
                    Common.toppingAdded.remove(buttonView!!.text.toString())
                    Common.toppingPrice -= (optionList[position].Price)!!.toDouble()
                }
            }
        })
    }

    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)
    {
        internal var cb_topping:CheckBox?=null
        init {
            cb_topping=itemView.findViewById(R.id.cb_topping)
        }
    }
}