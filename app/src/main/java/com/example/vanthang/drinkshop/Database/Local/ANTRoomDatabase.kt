package com.example.vanthang.drinkshop.Database.Local


import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.example.vanthang.drinkshop.Database.Local.ANTRoomDatabase.Companion.DATABASE_VERSION
import com.example.vanthang.drinkshop.Database.ModelDB.Cart
import com.example.vanthang.drinkshop.Database.ModelDB.Favorite


@Database(entities = arrayOf(Cart::class,Favorite::class),version = DATABASE_VERSION,exportSchema = false)
abstract class ANTRoomDatabase:RoomDatabase (){
    abstract fun cartDAO():CartDAO
    abstract fun favoriteDAO():FavoriteDAO
    companion object {
        const  val DATABASE_VERSION=1
        val DATABASE_NAME="DrinkShopDB"
        private var instance:ANTRoomDatabase?=null
        fun getInstance(context: Context):ANTRoomDatabase
        {
            if (instance==null)
                instance=Room.databaseBuilder(context,ANTRoomDatabase::class.java, DATABASE_NAME)
                        .allowMainThreadQueries()
                        .build()
            return instance!!
        }
    }
}