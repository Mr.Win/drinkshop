package com.example.vanthang.drinkshop.Services

import android.app.Notification
import android.app.NotificationManager
import android.content.Context
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.Common.NotificationHelper
import com.example.vanthang.drinkshop.R
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class MyFirebaseMessaging :FirebaseMessagingService(){

    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)
        if (Common.currentUser !=null)
        {
            updateTokenToFirebase(p0!!)
        }
    }
    private fun updateTokenToFirebase(token:String) {
        var mService=Common.getAPI()
        mService.updateToken(Common.currentUser!!.phone!!,token,"0")
                .enqueue(object : Callback<String> {
                    override fun onFailure(call: Call<String>, t: Throwable) {
                        Log.d("DEBUG",t.message)
                    }

                    override fun onResponse(call: Call<String>, response: Response<String>) {
                        Log.d("DEBUG",response.toString())
                    }
                })
    }
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)
        if (remoteMessage!!.data !=null)
        {
            if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.O)
                sendNotificationAPI27(remoteMessage)
            else
                sendNotification(remoteMessage)
        }
    }

    private fun sendNotification(remoteMessage: RemoteMessage) {
        //Get information from Message
        var data:Map<String,String> = remoteMessage.data
        var title:String= data.get("title")!!
        var message:String= data.get("message")!!

        var defaultSoundUri=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        var builder:NotificationCompat.Builder=NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
        var noti:NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        noti.notify(Random().nextInt(),builder.build())

    }

    private fun sendNotificationAPI27(remoteMessage: RemoteMessage) {
        //Get information from Message
        var data:Map<String,String> = remoteMessage.data
        var title:String= data.get("title")!!
        var message:String= data.get("message")!!
        //From API level 26,we need implement Notification chanel
        var helper:NotificationHelper
        var builder:Notification.Builder

        var defaultSoundUri=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        helper= NotificationHelper(this)
        builder=helper.getDrinkShopNotification(title,message,defaultSoundUri)

        helper.getManager().notify(Random().nextInt(),builder.build())

    }
}