package com.example.vanthang.drinkshop

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.example.vanthang.drinkshop.Adapter.DrinkAdapter
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.Model.Drinks
import com.example.vanthang.drinkshop.Retrofit.IDrinkShopAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_drink.*

class DrinkActivity : AppCompatActivity() {

    lateinit var mService: IDrinkShopAPI

    //Rxjava
    var compositeDisposable: CompositeDisposable = CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drink)

        mService= Common.getAPI()

        swipeRefreshLayout_drink.post(object :Runnable{
            override fun run() {
                swipeRefreshLayout_drink.isRefreshing=true
                txt_name_category.text=Common.currentCategory!!.Name
                loadListDrink(Common.currentCategory!!.ID)
            }
        })
        swipeRefreshLayout_drink.setOnRefreshListener(object :SwipeRefreshLayout.OnRefreshListener{
            override fun onRefresh() {
                swipeRefreshLayout_drink.isRefreshing=true
                txt_name_category.text=Common.currentCategory!!.Name
                loadListDrink(Common.currentCategory!!.ID)
            }
        })
        //show drinks
        rw_drink.layoutManager= GridLayoutManager(this@DrinkActivity,2)
        rw_drink.hasFixedSize()

    }

    private fun loadListDrink(menuid: String?) {
        compositeDisposable.add(mService.getDrink(menuid!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :Consumer<List<Drinks>>{
                    override fun accept(drinks: List<Drinks>?) {
                        displayListDrink(drinks!!)
                    }

                }))
    }
    private fun displayListDrink(drinks: List<Drinks>) {
        var adapter=DrinkAdapter(this@DrinkActivity,drinks )
        rw_drink.adapter=adapter
        swipeRefreshLayout_drink.isRefreshing=false
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }

}

