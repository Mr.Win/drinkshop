package com.example.vanthang.drinkshop.Database.Local

import android.arch.persistence.room.*
import com.example.vanthang.drinkshop.Database.ModelDB.Cart
import io.reactivex.Flowable




@Dao
interface CartDAO {
    @Query("SELECT * FROM Cart")
    fun getCartItems(): Flowable<List<Cart>>

    @Query("SELECT * FROM Cart WHERE id=:cartItemId")
    fun getCartItemById(cartItemId:Int): Flowable<List<Cart>>

    @Query("SELECT COUNT(*) FROM Cart ")
    fun countCartItems():Int

    @Query("SELECT SUM(price) FROM Cart ")
    fun sumPrice():Float


    @Query("DELETE FROM Cart")
    fun emptyCart()

    @Insert
    fun insertToCart(vararg carts: Cart)

    @Update
    fun updateToCart(vararg carts: Cart)

    @Delete
    fun deleteCartItem(carts: Cart)
}