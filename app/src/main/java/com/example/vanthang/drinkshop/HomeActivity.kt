package com.example.vanthang.drinkshop

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.example.vanthang.drinkshop.Adapter.CategoryAdapter
import com.example.vanthang.drinkshop.Common.Common
import com.example.vanthang.drinkshop.Common.ProgressRequestBody
import com.example.vanthang.drinkshop.Common.UploadCallBack
import com.example.vanthang.drinkshop.Database.DataSource.CartRepository
import com.example.vanthang.drinkshop.Database.DataSource.FavoriteRepository
import com.example.vanthang.drinkshop.Database.Local.ANTRoomDatabase
import com.example.vanthang.drinkshop.Database.Local.CartDataSource
import com.example.vanthang.drinkshop.Database.Local.FavoriteDataSource
import com.example.vanthang.drinkshop.Model.Banner
import com.example.vanthang.drinkshop.Model.Category
import com.example.vanthang.drinkshop.Model.Drinks
import com.example.vanthang.drinkshop.Retrofit.IDrinkShopAPI
import com.facebook.accountkit.AccountKit
import com.ipaulpro.afilechooser.utils.FileUtils
import com.nex3z.notificationbadge.NotificationBadge
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.content_home.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Response
import java.io.File
import java.lang.StringBuilder


class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, UploadCallBack {
    override fun onProgressUpdate(pertantage: Long) {

    }

    lateinit var txt_name: TextView
    lateinit var txt_phone: TextView
    lateinit var mService: IDrinkShopAPI
    var badge: NotificationBadge? = null
    lateinit var cart_icon: ImageView
    var img_avatar: CircleImageView? = null
    internal var PICK_FILE_REQUEST = 1002
    var selectedUri: Uri? = null


    //Rxjava
    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        mService = Common.getAPI()




        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        var headerView: View = nav_view.getHeaderView(0)
        txt_name = headerView.findViewById(R.id.txt_name) as TextView
        txt_phone = headerView.findViewById(R.id.txt_phone) as TextView
        img_avatar = headerView.findViewById(R.id.img_avatar) as CircleImageView

        //Event
        img_avatar!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                chooseImage()
            }
        })

        //set Infor
        txt_name.text = Common.currentUser!!.name
        txt_phone.text = Common.currentUser!!.phone

        //set Avatar
        if (!TextUtils.isEmpty(Common.currentUser!!.avatar)) {
            Picasso.with(this)
                    .load(StringBuilder(Common.BASE_URL)
                            .append("user_avatar/")
                            .append(Common.currentUser!!.avatar).toString())
                    .into(img_avatar)


        }

        swipeRefreshLayout.post(object :Runnable{
            override fun run() {
                //get Banners
                getBannerImage()

                //get newest topping list
                getToppingList()

                getMenu()

            }
        })
        swipeRefreshLayout.setOnRefreshListener(object :SwipeRefreshLayout.OnRefreshListener{
            override fun onRefresh() {
                swipeRefreshLayout.isRefreshing=true
                //get Banners
                getBannerImage()

                //get newest topping list
                getToppingList()

                getMenu()
            }
        })
        //show category
        rw_category.layoutManager = LinearLayoutManager(this@HomeActivity, LinearLayoutManager.HORIZONTAL, false)
        rw_category.hasFixedSize()

        //init DataBase()
        initDB()

    }

    private fun chooseImage() {
        startActivityForResult(Intent.createChooser(FileUtils.createGetContentIntent(), "select a file"),
                PICK_FILE_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FILE_REQUEST && data != null) {

                selectedUri = data!!.data
                if (selectedUri != null && !selectedUri!!.path.isEmpty()) {
                    img_avatar!!.setImageURI(selectedUri)
                    uploadFile()
                } else
                    Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show()

            }
        }
    }

    private fun uploadFile() {
        if (selectedUri != null) {
            var file: File = FileUtils.getFile(this@HomeActivity, selectedUri)
            var fileName: String = StringBuilder(Common.currentUser!!.phone)
                    .append(FileUtils.getExtension(file.toString()))
                    .toString()

            var requestFile: ProgressRequestBody = ProgressRequestBody(file, this@HomeActivity)
            var body: MultipartBody.Part = MultipartBody.Part.createFormData("uploaded_file", fileName, requestFile)
            var userPhone: MultipartBody.Part = MultipartBody.Part.createFormData("phone", Common.currentUser!!.phone)

            Thread(object : Runnable {
                override fun run() {
                    mService.uploadFile(userPhone, body)
                            .enqueue(object : retrofit2.Callback<String> {
                                override fun onFailure(call: Call<String>, t: Throwable) {
                                    Toast.makeText(this@HomeActivity, "xxxx"+t.message, Toast.LENGTH_SHORT).show()
                                }

                                override fun onResponse(call: Call<String>, response: Response<String>) {
                                    Toast.makeText(this@HomeActivity, "ok"+response.body(), Toast.LENGTH_SHORT).show()
                                }
                            })
                }
            }).start()
        }
    }

    private fun initDB() {
        Common.ANTRoomDatabase = ANTRoomDatabase.getInstance(this@HomeActivity)
        Common.cartRepository = CartRepository.getInstance(CartDataSource.getInstance(Common.ANTRoomDatabase!!.cartDAO()))
        Common.favoriteRepository = FavoriteRepository.getInstance(FavoriteDataSource.getInstance(Common.ANTRoomDatabase!!.favoriteDAO()))
    }

    private fun getToppingList() {
        compositeDisposable.add(mService.getDrink(Common.TOPPING_MENU_ID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Consumer<List<Drinks>> {
                    override fun accept(drinks: List<Drinks>?) {
                        Common.toppingList = drinks!!
                    }

                }))
    }

    private fun getMenu() {
        compositeDisposable.add(mService.getMenu()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Consumer<List<Category>> {
                    override fun accept(categories: List<Category>?) {
                        displayMenu(categories!!)
                    }

                }))
    }

    private fun displayMenu(categories: List<Category>) {
        var adapter = CategoryAdapter(this@HomeActivity, categories)
        rw_category.adapter = adapter
        swipeRefreshLayout.isRefreshing=false
    }

    private fun getBannerImage() {
        compositeDisposable.add(mService.getBanners()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Consumer<List<Banner>> {
                    override fun accept(banner: List<Banner>?) {
                        displayImage(banner!!)
                    }

                }))
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }

    private fun displayImage(banner: List<Banner>) {
        var bannerMap = HashMap<String, String>()
        for (item in banner) {
            bannerMap.put(item.Name!!, item.Link!!)
        }
        for (name in bannerMap.keys) {
            var textSlideView = TextSliderView(this@HomeActivity)
            textSlideView.description(name)
                    .image(bannerMap.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
            slider.addSlider(textSlideView)
        }
    }

    // exit application when click BACK button
    var isBackButtonClicked=false

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            if(isBackButtonClicked)
            {
                super.onBackPressed()
                return
            }
            this.isBackButtonClicked=true
            Toast.makeText(this@HomeActivity,"Please click BACK again to exit",Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_action_bar, menu)
        var view: View = menu.findItem(R.id.cart_menu).actionView
        badge = view.findViewById(R.id.badge)
        cart_icon = view.findViewById(R.id.cart_icon)
        cart_icon.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                startActivity(Intent(this@HomeActivity, CartActivity::class.java))
            }
        })
        updateCartCount()
        return true
    }

    private fun updateCartCount() {
        if (badge == null) return
        runOnUiThread(object : Runnable {
            override fun run() {
                if (Common.cartRepository!!.countCartItems() == 0)
                    badge!!.visibility = View.INVISIBLE
                else {
                    badge!!.visibility = View.VISIBLE
                    badge!!.setText(Common.cartRepository!!.countCartItems().toString())
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.cart_menu -> return true
            R.id.search_menu -> {
                startActivity(Intent(this@HomeActivity,SearchActivity::class.java))
                return true
            }
            else -> return super.onOptionsItemSelected(item)

        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_sign_out -> {
                // Handle the sign out action
                var builder: AlertDialog.Builder =AlertDialog.Builder(this@HomeActivity)
                builder.setTitle("Exit Application")
                builder.setMessage("Do you want to exit application ?")
                builder.setNegativeButton("OK",object :DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        AccountKit.logOut()
                        //clear all activity
                        var intent=Intent(this@HomeActivity,MainActivity::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                        finish()
                    }
                })
                builder.setPositiveButton("CANCEL",object :DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        dialog!!.dismiss()
                    }
                })
                builder.show()
            }
            R.id.nav_favorite_list -> {
                startActivity(Intent(this@HomeActivity,FavoriteActivity::class.java))
            }
            R.id.nav_show_orders->
            {
                startActivity(Intent(this@HomeActivity,ShowOrderActivity::class.java))
            }
            R.id.nav_show_store ->
                startActivity(Intent(this@HomeActivity,NearbyStore::class.java))
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onResume() {
        updateCartCount()
        isBackButtonClicked=false
        super.onResume()
    }

}
